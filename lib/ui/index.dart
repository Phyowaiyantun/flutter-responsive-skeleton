import 'package:flutter/material.dart';
import 'package:flutter_responsive_test/ui/responsive/responsive_layout.dart';
import 'package:flutter_responsive_test/ui/views/desktop/desktop_view.dart';
import 'package:flutter_responsive_test/ui/views/mobile/mobile_view.dart';
import 'package:flutter_responsive_test/ui/views/tablet/tablet_view.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var currentWidth = MediaQuery.of(context).size.width;
    return ResponsiveLayout(
      // tabletEndpoint: true,
      mobileEndpointWidth: 900,
      mobileBody: MobileBody(),
      desktopBody: DesktopBody(),
    );
  }
}
