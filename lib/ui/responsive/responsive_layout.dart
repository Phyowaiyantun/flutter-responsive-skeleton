import 'package:flutter/cupertino.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget mobileBody;
  final Widget? tabletBody;
  final Widget desktopBody;
  final Widget? tvBody;
  final bool mobileEndpoint;
  final bool tabletEndpoint;
  final bool desktopEndpoint;
  final bool tvEndpoint;
  final double mobileEndpointWidth;
  final double tabletEndpointWidth;
  final double desktopEndpointWidth;
  const ResponsiveLayout({
    Key? key,
    required this.mobileBody,
    required this.desktopBody,
    this.tabletBody,
    this.tvBody,
    this.mobileEndpoint = true,
    this.tabletEndpoint = false,
    this.desktopEndpoint = true,
    this.tvEndpoint = false,
    this.mobileEndpointWidth = 480,
    this.tabletEndpointWidth = 1024,
    this.desktopEndpointWidth = 1600,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < mobileEndpointWidth && mobileEndpoint) {
          return mobileBody;
        } else if (constraints.maxWidth < tabletEndpointWidth &&
            tabletEndpoint) {
          return tabletBody!;
        } else if (constraints.maxWidth < desktopEndpointWidth &&
            desktopEndpoint) {
          return desktopBody;
        } else if (tvEndpoint) {
          return tvBody!;
        } else {
          throw "OMG! The endpoint is not valid with all of the available points. Please recheck the options if you haven't set the current resolution endpoint please use this option.";
        }
      },
    );
  }
}
